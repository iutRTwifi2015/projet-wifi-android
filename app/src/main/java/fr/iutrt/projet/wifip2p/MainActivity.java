package fr.iutrt.projet.wifip2p;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import fr.iutrt.projet.wifip2p.ListeAppareilsFragment.DeviceActionListener;

public class MainActivity extends ActionBarActivity implements OnClickListener, WifiP2pManager.ChannelListener, DeviceActionListener  {

		GestionAppli gestionAppli = new GestionAppli(this);
        public static final String TAG = "WifiP2P";
        private BroadcastReceiver receiver = null;
		WifiManager wifiManager;
		WifiP2pManager wifiP2PManager;
		Channel channel;
		BroadcastReceiver br;
		final IntentFilter intentFilter = new IntentFilter();
        boolean isWifiP2pEnabled = false;

    public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
        this.isWifiP2pEnabled = isWifiP2pEnabled;
    }
    @Override
    public void onChannelDisconnected() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //  Indicates a change in the Wi-Fi P2P status.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        // Indicates a change in the list of available peers.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        // Indicates the state of Wi-Fi P2P connectivity has changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        // Indicates this device's details have changed.
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        //Initialisation de ce dont nous avons besoin
        wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        wifiP2PManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = wifiP2PManager.initialize(this, getMainLooper(), null);

    }

    public void reinitDonnees() {
        ListeAppareilsFragment fragmentList = (ListeAppareilsFragment) getFragmentManager()
                .findFragmentById(R.id.frag_list);
        DetailsAppareilsFragment fragmentDetails = (DetailsAppareilsFragment) getFragmentManager()
                .findFragmentById(R.id.frag_detail);
        if (fragmentList != null) {
            fragmentList.clearPeers();
        }
        if (fragmentDetails != null) {
            fragmentDetails.resetViews();
        }
    }
    
    @Override
    public void afficherDetails(WifiP2pDevice device) {
        DetailsAppareilsFragment fragment = (DetailsAppareilsFragment) getFragmentManager()
                .findFragmentById(R.id.frag_detail);
        fragment.showDetails(device);

    }
    @Override
    public void connect(WifiP2pConfig config) {
        wifiP2PManager.connect(channel, config, new ActionListener() {

            @Override
            public void onSuccess() {
                // RecepteurBroadcastWiFiDirect will notify us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(MainActivity.this, "La connexion a échoué. Veuillez Réessayer.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void disconnect() {
        final DetailsAppareilsFragment fragment = (DetailsAppareilsFragment) getFragmentManager()
                .findFragmentById(R.id.frag_detail);
        fragment.resetViews();
        wifiP2PManager.removeGroup(channel, new ActionListener() {

            @Override
            public void onFailure(int reasonCode) {
                Log.d(TAG, "La Déconnection a échoué, raison :" + reasonCode);
            }

            @Override
            public void onSuccess() {
                fragment.getView().setVisibility(View.GONE);
            }

        });
    }
    @Override
    public void annulerDeconnexion() {

        if (wifiP2PManager != null) {
            final ListeAppareilsFragment fragment = (ListeAppareilsFragment) getFragmentManager()
                    .findFragmentById(R.id.frag_list);
            if (fragment.getDevice() == null
                    || fragment.getDevice().status == WifiP2pDevice.CONNECTED) {
                disconnect();
            } else if (fragment.getDevice().status == WifiP2pDevice.AVAILABLE
                    || fragment.getDevice().status == WifiP2pDevice.INVITED) {

                wifiP2PManager.cancelConnect(channel, new ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(MainActivity.this, "Annulation de la connexion",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(MainActivity.this,
                                "Erreur lors de l'arrêt de la connexion, raison : " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId(); //On récupère l'id de l'élément du menu cliqué.
        //L'utilisateur a cliqué sur Paramètres, on affiche les paramètres réseaux wifi
        if (id == R.id.action_settings)
        {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        }
        if(id == R.id.action_scan_wifi)
        {
            final ListeAppareilsFragment fragment = (ListeAppareilsFragment) getFragmentManager()
                    .findFragmentById(R.id.frag_list);
            fragment.onInitiateDiscovery();
            wifiP2PManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

                @Override
                public void onSuccess() {
                    Toast.makeText(MainActivity.this, "Recherche Initialisé",
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(int reasonCode) {
                    String raison=null;
                    switch(reasonCode)
                    {
                        case 2:
                            raison="WiFi Désactivé";
                        break;
                        case 9:
                            raison="Privilèges Insuffisants";
                            break;
                        case 5:
                            raison="Arrêt prématuré de l'opération";
                            break;

                        default:
                            raison = "inconnue";
                        break;
                    }
                    Toast.makeText(MainActivity.this, "La Recherche a échoué, raison : " + raison,
                        Toast.LENGTH_SHORT).show();
                }
            });
        }
        //L'utilisateur a cliqué sur quitter, on kill l'appli !
        if(id == R.id.quitter)
        {
        	gestionAppli.quitter();
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	public void onClick(View v) {
        //Bouton n'existant plus : amélioration : bouton dans la barre d'actions.
        //L'utilisateur a cliqué sur le bouton pour changer l'état du wifi.
        /*if(v.getId() == R.id.toggleWifi)
        {
            if(wifiManager.isWifiEnabled())
                wifiManager.setWifiEnabled(false);
            else
                wifiManager.setWifiEnabled(true);
        } */
	}

    @Override
    public void onResume() {
        super.onResume();
        receiver = new RecepteurBroadcastWiFiDirect(wifiP2PManager, channel, this);
        registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}

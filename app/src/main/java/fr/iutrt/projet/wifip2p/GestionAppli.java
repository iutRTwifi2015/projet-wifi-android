package fr.iutrt.projet.wifip2p;

public final class GestionAppli {

	static MainActivity activity;
	
	GestionAppli(MainActivity c)
	{
		this.activity = c;
	}
	
	//Cette méthode permet de kill l'application.
	public void quitter()
	{
		activity.finish();
		System.exit(0);
	}
}

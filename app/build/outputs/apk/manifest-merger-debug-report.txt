-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:4:29
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:6:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:7:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.ACCESS_WIFI_STATE
ADDED from AndroidManifest.xml:9:2
	android:name
		ADDED from AndroidManifest.xml:9:19
uses-permission#android.permission.CHANGE_WIFI_STATE
ADDED from AndroidManifest.xml:10:2
	android:name
		ADDED from AndroidManifest.xml:10:19
uses-permission#android.permission.CHANGE_NETWORK_STATE
ADDED from AndroidManifest.xml:11:2
	android:name
		ADDED from AndroidManifest.xml:11:19
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:12:2
	android:name
		ADDED from AndroidManifest.xml:12:19
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:14:2
	android:name
		ADDED from AndroidManifest.xml:14:19
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
application
ADDED from AndroidManifest.xml:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:19:9
	android:allowBackup
		ADDED from AndroidManifest.xml:17:9
	android:icon
		ADDED from AndroidManifest.xml:18:9
	android:theme
		ADDED from AndroidManifest.xml:20:9
activity#fr.iutrt.projet.wifip2p.MainActivity
ADDED from AndroidManifest.xml:21:9
	android:label
		ADDED from AndroidManifest.xml:23:13
	android:name
		ADDED from AndroidManifest.xml:22:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:24:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:25:17
	android:name
		ADDED from AndroidManifest.xml:25:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:27:17
	android:name
		ADDED from AndroidManifest.xml:27:27
service#fr.iutrt.projet.wifip2p.ServiceTransfertFichiers
ADDED from AndroidManifest.xml:31:9
	android:enabled
		ADDED from AndroidManifest.xml:31:18
	android:name
		ADDED from AndroidManifest.xml:31:41
